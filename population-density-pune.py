# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 17:20:06 2020

@author: -Nirmitk Tripathii
"""

import geojson
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
#Reading the xcel file

df = pd.read_excel('PMC Population breakup by 15 administrative wards-2.xlsx', sheet_name ='41 ward + 11 village ')

#Opening the json file

with open("ward boundaries.json") as json_file:
    pune_wards = geojson.load(json_file)

#renaming and adjusting the columns of excel file     

df.rename(columns = {'पुणे महानगरपालिका सार्वत्रिक निवडणूक 2017  नूसार  प्रभाग निहाय लोकसंख्या  ': 'अ.क्र' ,
                   'Unnamed: 1': 'प्रभागाचे नाव',
                   'Unnamed: 2': 'प्रभाग क्रमांक ',
                   'Unnamed: 3': 'एकूण लोकसंख्या',
                   'Unnamed: 4': 'क्षेत्रिय कार्यालयाचे नाव',
                   'Unnamed: 5': 'परिमंडळ क्र '}, inplace = True)
df = df.shift(-1)

#reading the geojson file for Pune

df1 = gpd.read_file("Wards2.geojson")
df = pd.concat((df1,df),axis = 1) #Joining the 2 dataframes with population and wards together columnwise
df = df.drop(df.index[41:54]) #Dropping off the last rows with NAs 
df = df.drop(['प्रभागाचे नाव', 'प्रभाग क्रमांक ', 'क्षेत्रिय कार्यालयाचे नाव', 'परिमंडळ क्र '], axis = 1) #Dropping off redundant columns
df = df.to_crs({'init': 'epsg:32643'}) #Converting into Mercaptor projections
df['Area'] = df.area/10**6 #Finding the Area
df['Population density'] = (df['एकूण लोकसंख्या' ]/df['Area']) #Finding Pop Density
vmax = max(df['Population density']) #Finding max pop density

# df.plot(column = 'Population density', cmap = 'Spectral')

#Creating chloropeth map scheme on population density of Pune with continuous colorbar legend

from mpl_toolkits.axes_grid1 import make_axes_locatable 
fig, ax = plt.subplots(1, figsize =(16, 8), 
					facecolor ='lightblue') 

df.plot(ax = ax, color ='black') 
df.plot(ax = ax, column ='Population density', cmap ='Reds', 
		edgecolors ='grey') 

# axis for the color bar 

div = make_axes_locatable(ax) 
cax = div.append_axes("right", size ="3 %", pad = 0.05) 

# color bar 

mappable = plt.cm.ScalarMappable(cmap ='Reds', 
								norm = plt.Normalize(vmin = 0, vmax = vmax)) 
cbar = fig.colorbar(mappable, cax, label = 'Population Density of Pune') 

ax.axis('off') 
plt.show() 
